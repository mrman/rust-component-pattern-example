# rust-component-pattern-example

This repository contains an example implementation of the component pattern (similar to [stuartsierra/component](https://github.com/stuartsierra/component)), including examples for request/response communication and building bigger trait hierarchies.
