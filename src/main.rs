extern crate rust_component_pattern_example;
extern crate time;

use std::time::Duration;
use std::thread;
use std::sync::mpsc::channel;
use rust_component_pattern_example::components::clock::{spawn as spawn_clock, Clock, ClockRequest};
use rust_component_pattern_example::components::{ComponentRequest, ComponentLifecycleRequest, RequestResponse};

fn main() {
    println!("Spawning two clock components...");
    let (clock_one_handle, clock_one_tx) = spawn_clock( || Clock::new()).expect("failed to spawn clock one");
    let (clock_two_handle, clock_two_tx) = spawn_clock( || Clock::new()).expect("failed to spawn clock two");

    // Send GetCurrentTime to clock_one, synchronously wait for a response
    println!("\nGetting clock one's time synchronously (blocking main thread waiting on response)...");
    let (resp_tx, resp_rx) = channel();
    clock_one_tx.send(
        ComponentRequest::Operation(
            RequestResponse {
                req: ClockRequest::GetCurrentTime,
                resp_chan: resp_tx
            }
        )
    ).expect("failed to send to clock one");
    let result = resp_rx.recv();
    println!("Received result from clock_one: {:?}", result);

    println!("\nGetting clock two's time asynchronously from another thread after three seconds...");
    let cloned_clock_two_tx = clock_two_tx.clone();
    let async_work = thread::spawn(move || {
        // Wait 3 seconds
        thread::sleep(Duration::from_secs(3));

        // Send GetCurrentTime to clock_two return the result
        let (resp_tx, resp_rx) = channel();
        cloned_clock_two_tx.send(
            ComponentRequest::Operation(
                RequestResponse {
                    req: ClockRequest::GetCurrentTime,
                    resp_chan: resp_tx
                }
            )
        ).expect("failed to send to clock_two from thread");

        let result = resp_rx.recv();
        println!("Received result from clock_two: {:?}", result);
    });


    // Shutdown both components after waiting 5 seconds
    thread::spawn(move || {
        thread::sleep(Duration::from_secs(5));
        println!("\nSending shutdown to both clock components!");
        let _ = clock_one_tx.send(ComponentRequest::Lifecycle(ComponentLifecycleRequest::Shutdown)).expect("failed to send shutdown");
        let _ = clock_two_tx.send(ComponentRequest::Lifecycle(ComponentLifecycleRequest::Shutdown)).expect("failed to send shutdown");
    });

    // Wait for both threads to finish
    println!("\nthread::join on async work and both clock handles...");
    let _ = async_work.join().expect("failed to join work thread");
    let _ = clock_one_handle.join().expect("failed to join clock one thread");
    let _ = clock_two_handle.join().expect("failed to join clock two thread");
    println!("DONE!");
}
