use std::thread::JoinHandle;
use std::thread;
use std::sync::mpsc::{Receiver, Sender};
use components::*;
use time::now_utc;
use std::sync::mpsc::channel;
use simple_signal::{Signal, set_handler as set_signal_handler};

#[derive(Debug)]
pub enum ClockRequest {
    GetCurrentTime,
    GetCurrentFormatString,
    ReplaceFormatString(String)
}

#[derive(Debug)]
pub struct Clock {
    clock_cfg: ClockCfg,

    cmd_bus_tx: Sender<ComponentRequest<ClockRequest>>,
    cmd_bus_rx: Receiver<ComponentRequest<ClockRequest>>
}

#[derive(Debug)]
pub struct ClockCfg {
    format_str: String,
}

impl ClockCfg {
    pub fn new() -> ClockCfg {
        ClockCfg {
            format_str: String::from("%Y-%m-%d %H:%M:%S")
        }
    }
}

impl Clock {
    pub fn new() -> Clock {
        let (cmd_bus_tx, cmd_bus_rx) = channel();
        let clock_cfg = ClockCfg::new();
        Clock {
            clock_cfg,
            cmd_bus_tx,
            cmd_bus_rx
        }
    }
}

impl Component for Clock {
    fn get_name(&self) -> &'static str { "clock" }

    fn start(&mut self) -> Result<(), ComponentError> {
        // Control loop
        loop {
            let msg = self.recv_request()?;

            match msg {
                // Lifecycle handling
                ComponentRequest::Lifecycle(req) => {
                    match req {
                        ComponentLifecycleRequest::Shutdown => { break }
                    }
                },

                // Simple request-response handling
                ComponentRequest::Operation(req) => {
                    // Maybe we should do something with the result below...
                    let _ = self.handle_simple_req(req)?;
                    thread::yield_now();
                }
            }
        }

        Ok(())
    }

    fn stop(&self) -> Result<(), ComponentError> {
        // TODO: send message to stop to the component
        Ok(())
    }
}

impl Configurable<ClockCfg> for Clock {
    fn update_config(&mut self, updated: Option<ClockCfg>) -> Result<(), ComponentError> {
        if let Some(new_cfg) = updated {
            self.clock_cfg = new_cfg
        }

        Ok(())
    }

    fn get_config(&self) -> &ClockCfg { &self.clock_cfg }
}

impl HandlesSimpleRequests for Clock {
    type Request = ClockRequest;

    fn recv_request(&self) -> Result<ComponentRequest<ClockRequest>, ComponentError> {
        self.cmd_bus_rx.recv().map_err(ComponentError::from)
    }

    fn get_request_chan(&self) -> Result<Sender<ComponentRequest<ClockRequest>>, ComponentError> {
        Ok(self.cmd_bus_tx.clone())
    }

    fn handle_simple_req(&mut self, envelope: RequestResponse<ClockRequest>) -> Result<(), ComponentError> {
        match envelope {
            // handle GetCurrentTime
            RequestResponse { req: ClockRequest::GetCurrentTime, resp_chan: resp } => {
                let _ = resp.send(Some(Box::new(now_utc())));
                Ok(())
            }

            // handle GetCurrentFormatString
            RequestResponse { req: ClockRequest::GetCurrentFormatString, resp_chan: resp } => {
                let _ = resp.send(Some(Box::new(self.clock_cfg.format_str.clone())));
                Ok(())
            }

            // handle ReplaceFormatString
            RequestResponse { req: ClockRequest::ReplaceFormatString(new_str), resp_chan: _ } => {
                self.clock_cfg.format_str = new_str;
                Ok(())
            }
        }
    }
}

pub fn spawn<F: 'static>(constructor: F) -> Result<(JoinHandle<Result<(), ComponentError>>, Sender<ComponentRequest<ClockRequest>>), ComponentError>
where
    F: Fn() -> Clock + Send,
{

    let (tx, rx) = channel();
    let cloned_tx = tx.clone();

    let handle = thread::spawn(move || {
        let mut c = constructor();
        c.cmd_bus_tx = cloned_tx;
        c.cmd_bus_rx = rx;

        // Set up SIGINT handling for this clock component in particular
        let cloned_tx = c.cmd_bus_tx.clone();
        set_signal_handler(&[Signal::Int, Signal::Term], move |_signals| {
            println!("clock component received SIGINT/SIGTERM!");
            cloned_tx.send(ComponentRequest::Lifecycle(ComponentLifecycleRequest::Shutdown)).expect("failed to send shutdown");
        });

        let _ = c.start()?;

        Ok(())
    });

    Ok((handle, tx))
}
