pub mod clock;

use std::sync::mpsc::{Sender, RecvError};
use std::any::Any;

#[derive(Debug)]
pub enum ComponentError {
    NotStarted,
    NotImplemented,
    InvalidConfiguration,
    FailedMessageReceive(RecvError)
}

#[derive(Debug)]
pub enum ComponentLifecycleRequest {
    Shutdown
}

#[derive(Debug)]
pub enum ComponentRequest<T> {
    Lifecycle(ComponentLifecycleRequest),
    Operation(RequestResponse<T>)
}

pub trait Component: HandlesSimpleRequests {
    fn get_name(&self) -> &str;
    fn start(&mut self) -> Result<(), ComponentError>;
    fn stop(&self) -> Result<(), ComponentError>;
}

pub trait Configurable<C> where Self: Component {
    // Note that you could get around the &mut self here if you used a std::sync::Mutex wrapped around the config class (C)
    // handling the synchronization in the interior.
    fn update_config(&mut self, updated: Option<C>) -> Result<(), ComponentError>;
    fn get_config(&self) -> &C;
}

pub trait HandlesSimpleRequests {
    type Request;

    fn recv_request(&self) -> Result<ComponentRequest<Self::Request>, ComponentError>;
    fn get_request_chan(&self) -> Result<Sender<ComponentRequest<Self::Request>>, ComponentError>;
    fn handle_simple_req(&mut self, envelope: RequestResponse<Self::Request>) -> Result<(), ComponentError>;
}

#[derive(Debug)]
pub struct RequestResponse<RQ> {
    pub req: RQ,
    pub resp_chan: Sender<Option<Box<Any + Send>>>
}

impl From<RecvError> for ComponentError {
    fn from(e: RecvError) -> ComponentError { ComponentError::FailedMessageReceive(e) }
}
